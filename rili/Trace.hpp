#pragma once
#include <cstdint>

namespace rili {
namespace trace {
class ExecutionTracer {
 public:
    struct BlockMarker {
        char const* functionName;
        std::uint64_t line;
    };
    class TrackPoint {
     public:
        TrackPoint(BlockMarker const* block, bool isSleeping);
        ~TrackPoint();

     public:
        BlockMarker const* const block;
        std::uint64_t createTime;
        std::uint64_t sleepTime;
        bool isSleeping;
    };

 public:
    ExecutionTracer(ExecutionTracer const&) = delete;
    ExecutionTracer& operator=(ExecutionTracer const&) = delete;

    static void set(ExecutionTracer* impl);
    static ExecutionTracer* get();
    static void onEnter(TrackPoint* p);
    static void onExit(TrackPoint* p);
    static void store();

 protected:
    ExecutionTracer() = default;
    virtual ~ExecutionTracer() = default;

 protected:
    virtual void onEnterImpl(TrackPoint* p) = 0;
    virtual void onExitImpl(TrackPoint* p) = 0;
    virtual void storeImpl() const = 0;
};
}  // namespace trace
}  // namespace rili

#ifndef _MSC_VER
#define RILI_TRACE()                                                                                   \
    static ::rili::trace::ExecutionTracer::BlockMarker RILI_TRACE_LINE_NAME(_riliTraceBlockMarker) = { \
        __PRETTY_FUNCTION__, __LINE__};                                                                \
    ::rili::trace::ExecutionTracer::TrackPoint RILI_TRACE_LINE_NAME(_riliTracePoint)(                  \
        &(RILI_TRACE_LINE_NAME(_riliTraceBlockMarker)), false)
#define RILI_TRACE_SLEEPING()                                                                          \
    static ::rili::trace::ExecutionTracer::BlockMarker RILI_TRACE_LINE_NAME(_riliTraceBlockMarker) = { \
        __PRETTY_FUNCTION__, __LINE__};                                                                \
    ::rili::trace::ExecutionTracer::TrackPoint RILI_TRACE_LINE_NAME(_riliTracePoint)(                  \
        &(RILI_TRACE_LINE_NAME(_riliTraceBlockMarker)), true)
#else
#define RILI_TRACE()                                                                                               \
    static ::rili::trace::ExecutionTracer::BlockMarker RILI_TRACE_LINE_NAME(_riliTraceBlockMarker) = {__FUNCSIG__, \
                                                                                                      __LINE__};   \
    ::rili::trace::ExecutionTracer::TrackPoint RILI_TRACE_LINE_NAME(_riliTracePoint)(                              \
        &(RILI_TRACE_LINE_NAME(_riliTraceBlockMarker)), false)
#define RILI_TRACE_SLEEPING()                                                                                      \
    static ::rili::trace::ExecutionTracer::BlockMarker RILI_TRACE_LINE_NAME(_riliTraceBlockMarker) = {__FUNCSIG__, \
                                                                                                      __LINE__};   \
    ::rili::trace::ExecutionTracer::TrackPoint RILI_TRACE_LINE_NAME(_riliTracePoint)(                              \
        &(RILI_TRACE_LINE_NAME(_riliTraceBlockMarker)), true)
#endif

#define RILI_TRACE_LINE_NAME(P) RILI_TRACE_JOIN(P, __LINE__)
#define RILI_TRACE_JOIN(S1, S2) RILI_TRACE_DO_JOIN(S1, S2)
#define RILI_TRACE_DO_JOIN(S1, S2) S1##S2
