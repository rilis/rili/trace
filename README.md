# Rili Trace

This is module of [rili](https://gitlab.com/rilis/rili) which provide basic trace profiler


**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/trace/badges/master/build.svg)](https://gitlab.com/rilis/rili/trace/commits/master)
