#pragma once
#include <atomic>
#include <cstdint>
#include <rili/Trace.hpp>

namespace rili {
namespace trace {
namespace detail {
struct D2SBuff {
    char buff[256];
};
D2SBuff d2S(double v);
}  // detail
typedef unsigned long long ull;  // NOLINT
template <typename ValueType, std::size_t Size>
class LockFreeHashMap {
 public:
    static_assert((Size & (Size - 1)) == 0, "Size must be power of 2");
    struct Node {
     public:
        Node() : key(0), value() {}

     public:
        std::atomic<std::uint64_t> key;
        ValueType value;
    };

 public:
    LockFreeHashMap() : storage() {}
    Node* getOrAcquire(std::uint64_t key) {
        auto index = hash(key);

        for (auto tries = 0u; tries < Size; tries++) {
            index &= Size - 1;
            std::uint64_t probedKey = storage[index].key.load();
            if (probedKey != key) {
                if (probedKey == 0) {
                    std::uint64_t prevKey = 0;
                    if (storage[index].key.compare_exchange_strong(prevKey, key) || prevKey == key) {
                        return &(storage[index]);
                    }
                }
            } else {
                return &(storage[index]);
            }
            index++;
        }
        return nullptr;
    }

 private:
    static std::uint32_t hash(std::uint64_t v) {
        auto h = static_cast<std::uint32_t>(v & 0xffffffff);
        h ^= h >> 16;
        h *= 0x85ebca6b;
        h ^= h >> 13;
        h *= 0xc2b2ae35;
        h ^= h >> 16;
        return h;
    }

 public:
    Node storage[Size];
};

template <std::size_t MaxNumberOfFunctions, std::size_t MaxStackDepth, std::size_t MaxNumberOfStacks,
          std::size_t MaxNumberOfChild>
class ExecutionTracerImpl : public ExecutionTracer {
 protected:
    ExecutionTracerImpl() : ExecutionTracer(), m_complete(0), m_counters(), m_stacks() {}
    virtual ~ExecutionTracerImpl() = default;

 protected:
    virtual std::uint64_t getTick() const = 0;
    virtual std::uint64_t numberOfTicksInSecond() const = 0;

    /**
     * @brief getCurrentThreadId - should return id of current thread. Returned value must not be equal to 0
     */
    virtual std::uint64_t getCurrentThreadId() const = 0;

    /**
     * @brief print - is used to store tracing results
     */
    virtual void print(const char* format, ...) const = 0;

    /**
     * @brief recordEnter is called when trace block starts execution. Should not be blocking otherwise will influence
     * execution time measurments
     */
    virtual void recordEnter(TrackPoint const* /*p*/) {}
    /**
     * @brief recordExit is called when trace block complete execution. Should not be blocking otherwise will influence
     * execution time measurments
     */
    virtual void recordExit(TrackPoint const* /*p*/) {}

 private:
    void onEnterImpl(TrackPoint* p) override {
        p->createTime = getTick();
        recordEnter(p);
        std::uint64_t tid = getCurrentThreadId();
        if (tid == 0) {
            onError(1);
            return;
        }
        auto* found = m_stacks.getOrAcquire(tid);

        if (found) {
            if (found->value.stackDepth < MaxStackDepth) {
                found->value.stack[found->value.stackDepth] = p;
                found->value.stackDepth++;
            } else {
                onError(2);
            }
        } else {
            onError(3);
        }
    }
    void onExitImpl(TrackPoint* p) override {
        std::uint64_t duration = 0;
        auto tid = getCurrentThreadId();
        std::uint64_t sleepTime = p->sleepTime;
        recordExit(p);
        if (tid == 0) {
            onError(4);
            return;
        }
        {
            auto destroyTime = getTick();
            duration = destroyTime - p->createTime;

            auto* found = m_counters.getOrAcquire(reinterpret_cast<std::uint64_t>(p->block));
            if (found) {
                auto& c = *found;
                c.value.isSleeping |= p->isSleeping;
                if (c.value.isSleeping) {
                    sleepTime += duration;
                }
                if (duration > 0) {
                    c.value.successCounter++;
                    c.value.sumDuration += duration;

                    if (c.value.minDuration == 0 || c.value.minDuration > duration) {
                        c.value.minDuration = duration;
                    }

                    if (c.value.maxDuration < duration) {
                        c.value.maxDuration = duration;
                    }
                } else {
                    c.value.failureCounter++;
                }
            } else {
                onError(5);
            }
        }
        {
            auto* found = m_stacks.getOrAcquire(tid);
            if (found) {
                if (found->value.stack[found->value.stackDepth - 1] == p) {
                    if (found->value.stackDepth >= 2) {
                        auto* parent = found->value.stack[found->value.stackDepth - 2];
                        parent->sleepTime += sleepTime;
                        updateCallGraph(parent->block, p->block, duration, sleepTime);
                    }
                    found->value.stackDepth--;
                } else {
                    onError(6);
                }
            } else {
                onError(7);
            }
        }
    }
    void storeImpl() const override {
        const auto ticksInSecond = static_cast<double>(numberOfTicksInSecond());
        auto toSeconds = [&ticksInSecond](std::uint64_t ticks) { return static_cast<double>(ticks) / ticksInSecond; };

        print("{\"status\":%llu,", ull(m_complete));
        print("\"nodes\":{");
        {
            auto isFirstParent = 0;
            for (auto const& n : m_counters.storage) {
                if (n.key != 0) {
                    PerformanceCounters const& c = n.value;
                    auto kv = n.key.load();
                    if (isFirstParent == 0) {
                        print(
                            "\"%llx\":{\"name\":\"%s@%llu\",\"isSleeping\":%s,\"duration\":%s,\"max\":%s,\"min\":"
                            "%s,"
                            "\"failure\":%llu,\"success\":%llu,\"child\":{",
                            ull(kv), reinterpret_cast<BlockMarker const*>(kv)->functionName,
                            ull(reinterpret_cast<BlockMarker const*>(kv)->line), (c.isSleeping ? "true" : "false"),

                            detail::d2S(toSeconds(c.sumDuration)).buff, detail::d2S(toSeconds(c.maxDuration)).buff,
                            detail::d2S(toSeconds(c.minDuration)).buff, ull(c.failureCounter), ull(c.successCounter));
                    } else {
                        print(
                            ",\"%llx\":{\"name\":\"%s@%llu\",\"isSleeping\":%s,\"duration\":%s,\"max\":%s,"
                            "\"min\":%s,"
                            "\"failure\":%llu,\"success\":%llu,\"child\":{",
                            ull(kv), reinterpret_cast<BlockMarker const*>(kv)->functionName,
                            ull(reinterpret_cast<BlockMarker const*>(kv)->line), (c.isSleeping ? "true" : "false"),

                            detail::d2S(toSeconds(c.sumDuration)).buff, detail::d2S(toSeconds(c.maxDuration)).buff,
                            detail::d2S(toSeconds(c.minDuration)).buff, ull(c.failureCounter), ull(c.successCounter));
                    }
                    auto isFirstChild = 0;
                    for (auto const& child : c.childs.storage) {
                        if (child.key != 0) {
                            auto ck = child.key.load();
                            if (isFirstChild == 0) {
                                print("\"%llx\":{\"count\":%llu,\"duration\":%s,\"sleep\":%s}", ull(ck),
                                      ull(child.value.count), detail::d2S(toSeconds(child.value.duration)).buff,
                                      detail::d2S(toSeconds(child.value.sleepDuration)).buff);
                            } else {
                                print(",\"%llx\":{\"count\":%llu,\"duration\":%s,\"sleep\":%s}", ull(ck),
                                      ull(child.value.count), detail::d2S(toSeconds(child.value.duration)).buff,
                                      detail::d2S(toSeconds(child.value.sleepDuration)).buff);
                            }
                            isFirstChild++;
                        }
                    }
                    print("}}");
                    isFirstParent++;
                }
            }
        }
        print("}}");
    }

 private:
    void updateCallGraph(BlockMarker const* parentBlock, BlockMarker const* childBlock, std::uint64_t childTime,
                         std::uint64_t sleepTime) {
        auto* found = m_counters.getOrAcquire(reinterpret_cast<std::uint64_t>(parentBlock));
        if (found) {
            auto* child = found->value.childs.getOrAcquire(reinterpret_cast<std::uint64_t>(childBlock));
            if (child) {
                child->value.count++;
                child->value.duration += childTime;
                child->value.sleepDuration += sleepTime;
                return;
            } else {
                onError(8);
            }
        }
        onError(9);
    }
    void onError(std::uint64_t source) {
        static bool gotIt = false;
        if (!gotIt) {
            gotIt = true;
            m_complete = source;
        }
    }

 private:
    struct PerformanceCounters {
        PerformanceCounters()
            : failureCounter(0),
              successCounter(0),
              sumDuration(0),
              minDuration(0),
              maxDuration(0),
              isSleeping(false),
              childs() {}
        struct Child {
            Child() : count(0), duration(0), sleepDuration(0) {}
            std::atomic<std::uint64_t> count;
            std::atomic<std::uint64_t> duration;
            std::atomic<std::uint64_t> sleepDuration;
        };
        std::atomic<std::uint64_t> failureCounter;
        std::atomic<std::uint64_t> successCounter;
        std::atomic<std::uint64_t> sumDuration;
        std::atomic<std::uint64_t> minDuration;
        std::atomic<std::uint64_t> maxDuration;
        bool isSleeping;

        LockFreeHashMap<Child, MaxNumberOfChild> childs;
    };

    struct ThreadContext {
        ThreadContext() : stackDepth(0), stack() {}
        std::uint64_t stackDepth;
        TrackPoint* stack[MaxStackDepth];
    };

 private:
    std::uint64_t m_complete;
    LockFreeHashMap<PerformanceCounters, MaxNumberOfFunctions> m_counters;
    LockFreeHashMap<ThreadContext, MaxNumberOfStacks> m_stacks;
    typedef typename LockFreeHashMap<ThreadContext, MaxNumberOfStacks>::Node ThreadNode;
};

typedef ExecutionTracerImpl<1024, 30, 1024, 32> DefaultTracerImpl;
}  // namespace trace
}  // namespace rili
